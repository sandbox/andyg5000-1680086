<?php
/**
 * @file
 * Default theme implementation to display a simple invoice.
 *
 * Available variables:
 * - $order: A commerce order object
 * - $commerce_products: An array of commerce product line items in the order
 * - $commerce_products_table: A pre-formatted line item table
 * - $commerce_shipping: An array of shipping line items in the order
 * - $order_totals: An array of order total values (subtotal, taxes, total)
 * - $order_balance: An array that contains the order balance
 * - $order_payments: An array of order payments
 */
?>
<div class="simple-invoice simple-invoice-<?php print $order->order_number; ?>">
  <div class="simple-invoice-order-info">
    <div class='simple-invoice-order-id'>
      <span class='label'>Order Number:</span> 
      <?php print $order->order_number; ?>
    </div>
    <div class='simple-invoice-order-date'>
      <span class='label'>Order Date:</span> 
      <?php print format_date($order->created, 'custom', 'M dS Y'); ?>
    </div>
  </div>
  <?php if (!empty($commerce_products_table)): ?>
    <div class="simple-invoice-products">
      <?php print $commerce_products_table; ?>
    </div>
  <?php endif; ?>
  <?php if (!empty($commerce_shipping_table)): ?>
    <div class="simple-invoice-shipping">
      <?php print $commerce_shipping_table; ?>
    </div>
  <?php endif; ?>
  <?php if (!empty($order_totals_table)): ?>
    <div class="simple-invoice-totals">
      <?php print $order_totals_table; ?>
    </div>
  <?php endif; ?>
  <?php if (!empty($order_payments_table)): ?>
    <div class="simple-invoice-payments">
      <?php print $order_payments_table; ?>
    </div>
  <?php endif; ?>
</div>